import express, {Application, Request, Response} from "express";
import morgan from 'morgan';
import cors from 'cors';

import IndexRoutes from './routes/indexRoutes';
import PetsRoutes from './routes/petsRoutes';
import UsersRoutes from './routes/userRoutes';
import admin from 'firebase-admin';

const serviceAccount = require("../../../serviceAccount.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://validacion-d842b.firebaseio.com"
});

class Server{
    
    public app: Application; 

    public indexRoutes: IndexRoutes;
    public petsRoutes: PetsRoutes;
    public usersRoutes: UsersRoutes;
    
    constructor(){
        this.indexRoutes = new IndexRoutes();
        this.petsRoutes = new PetsRoutes(admin);
        this.usersRoutes = new UsersRoutes(admin);
        this.app = express();
        this.config();
        this.routes();
    }

    config(): void{
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended:false}))
    }

    routes(): void{
        this.app.use("/", this.indexRoutes.router);
        this.app.use("/api/pets",this.petsRoutes.router);
        this.app.use("/api/users",this.usersRoutes.router);
    }

    start(): void{
        this.app.listen(this.app.get('port'), ()=>{
            console.log("Server on port: ", this.app.get('port'));
        });
    }
}

const server = new Server();
server.start();