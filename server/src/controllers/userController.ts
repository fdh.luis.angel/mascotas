import {Request, Response} from 'express';

class UserController{
    public index(req: Request, res: Response, admin:any){
        const db = admin.database();
        db.ref('Users').push('Random user');
        res.send('users :D')
    }

    public create(req: Request, res: Response, admin:any){
        const db = admin.database();
        res.json({response: "creating a user"})
    }

    public delete(req: Request, res: Response, admin:any){
        const db = admin.database();
        res.json({response: "Deleting user " + req.params.id})
    }

    public update(req: Request, res: Response, admin:any){
        const db = admin.database();
        res.json({response: "updating user " + req.params.id})
    }

    public read(req: Request, res: Response, admin:any){
        const db = admin.database();
        res.json({response: "user " + req.params.id})
    }
}

export default UserController;