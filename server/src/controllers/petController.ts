import {Request, Response} from 'express';

class PetController{
    public index(req: Request, res: Response, admin:any){
        const db = admin.database();

        res.send('Pets :D');
    }

    public create(req: Request, res: Response, admin:any){
        const db = admin.database();
        res.json({response: "creating a pet"})
    }

    public delete(req: Request, res: Response, admin:any){
        const db = admin.database();
        res.json({response: "Deleting pet "+ req.params.id})
    }

    public update(req: Request, res: Response, admin:any){
        const db = admin.database();
        
        res.json({response: "updating  pet "+ req.params.id})
    }

    public read(req: Request, res: Response, admin:any){
        const db = admin.database();
        res.json({response: "pet "+ req.params.id})
    }
}

export default PetController;