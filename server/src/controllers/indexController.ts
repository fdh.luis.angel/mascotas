import {Request, Response} from 'express';

class IndexController{
    public index(req: Request, res: Response){
        res.send('Index, working ok');
    }
}

export default IndexController;