import {Router} from 'express';
import UserController from '../controllers/userController'

class UserRoutes{
    router: Router = Router();
    userController:UserController;
    admin : any;

    constructor(admin:any){
        this.admin = admin;
        this.config();
        this.userController = new UserController();
    }

    config():void{
        this.router.get('/', (req, res)=>this.userController.index(req, res, this.admin));
        this.router.get('/:id', (req, res)=>this.userController.read(req, res, this.admin));
        this.router.post('/',(req, res)=> this.userController.create(req, res, this.admin));
        this.router.put('/:id', (req, res)=>this.userController.update(req, res, this.admin));
        this.router.delete('/:id', (req, res)=>this.userController.delete(req, res, this.admin));
    }
}

export default UserRoutes;
