import {Router} from 'express';
import IndexController from '../controllers/indexController';

class IndexRoutes{
    router: Router = Router();
    indexController: IndexController;

    constructor(){
        this.indexController = new IndexController();
        this.config();
    }

    config():void{
        this.router.get('/', this.indexController.index);
    }
}

export default IndexRoutes;
