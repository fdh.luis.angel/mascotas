import {Router} from 'express';
import PetController from '../controllers/petController';

class PetsRoutes{
    router: Router = Router();
    petController : PetController;
    admin : any;

    constructor(admin:any){
        this.admin = admin;
        this.config();
        this.petController = new PetController();
    }

    config():void{
        this.router.get('/', (req, res)=> this.petController.index(req, res, this.admin));
        this.router.get('/:id', (req, res)=>this.petController.read(req, res, this.admin));
        this.router.post('/', (req, res)=>this.petController.create(req, res, this.admin));
        this.router.put('/:id', (req, res)=>this.petController.update(req, res, this.admin));
        this.router.delete('/:id', (req, res)=>this.petController.delete(req, res, this.admin));
    }
}

export default PetsRoutes;
